# Chris Seifried
# 10/26/16
# Project 2

.data
		sequence:	.space	100	# Stores the current game's random sequence (RNG just-in-time)
	
.text
getNewGame:	add	$t9, $zero, $zero	# Reset input to zero
		bne	$t9, 16, getNewGame	# Check if user clicked new game button
		add	$t8, $zero, 16		# $t8 = 16, disable start button and play start sound
						# Set up RNG
		addi	$v0, $zero, 30		# Get system time to seed RNG
		syscall				# Get sys time
		add	$t0, $zero, $a0		# $t0 - system time
		addi 	$v0, $zero, 40		# For setting seed of RNG
		addi 	$a0, $zero, 1 		# RNG ID
		add  	$a1, $zero, $t0		# Use system time as seed
		syscall				# Set RNG seed to system time
		add	$s0, $zero, $zero	# $s0 = number of tones in sequence = 0
	
gameLoop:	la	$a0, sequence		# Load address of sequence block -> $a0
		add	$a1, $zero, $s0		# $a0 = $s0 = num of tones in sequence
		jal	_genNxtTone		# Generate next tone, store in sequence
		add	$s0, $s0, 1		# Increment number of tones
		la	$a0, sequence		# Load address of sequence block -> $a0
		jal	_playSequence		# Play the sequence of tones in "sequence" block
		la	$a0, sequence		# Load address of sequence block -> $a0
		jal	_userPlay		# Let user enter sequence
		add	$t0, $zero, $v0		# Copy return value to $t0
		beq	$t0, $zero, gameLoop	# If successful, start new round
		j	getNewGame		# Start new game if user loses
		
	
##-----------------------------------------------##
# _genNxtTone -  generate next color/tone, add it
#		 to end of data sequence block
# Args:		$a0 - addr of sequence
# 		$a1 - number of tones already in seq
# Returns:	none
#------------------------------------------------##	
_genNxtTone:	
		add	$t0, $zero, $a0		# Store addr of sequence in $t0	
		add	$t1, $zero, $a1		# Store number of tones in $t1
		addi 	$v0, $zero, 42		# Set for random num generation
		addi	$a0, $zero, 1		# RNG I.D.
		addi  	$a1, $zero, 4		# Want a number in [0,4)
		syscall				# Generate number from 0 to 3 inclusive
		add	$t2, $zero, $a0		# $t2 = random num
		addi	$t3, $zero, 1		# $t3 = 1
		sllv	$t3, $t3, $t2		# $t3 = color code (shift 1 left $t1 times)
		add	$t0, $t0, $t1		# Go to first free byte of sequence block
		sb  	$t3, ($t0) 		# Store $t3 there
		jr	$ra			# Return

##-----------------------------------------------##
# _playSequence - play tones in sequence
# Args:		$a0 - addr of sequence
# Returns:	none
#------------------------------------------------##
_playSequence:
		add	$t0, $zero, $a0		# $t0 = addr of sequence
		#add	$t8, $zero, $zero	# $t8 = 0, reset for output. Need this?? ###################
 pS_loop:	lb	$t1, ($t0)		# $t1 = current color code
 		beq	$t1, $zero, pS_done	# If current code == 0, we're done	
 pS_wait:	bne	$t8, $zero, pS_wait	# Wait for previous tone to end
 		add	$t8, $zero, $t1		# Add color code to $t8 for output
 		addi	$t0, $t0, 1		# Increment $t0 a.k.a. move on to next tone in seq.
 		j	pS_loop			# Loop through sequence
 pS_done:	jr	$ra			# Return

 		
##-----------------------------------------------##
# _userPlay - user inputs sequence, then compare
# Args:		$a0 - addr of sequence
# Returns:	$v0 - 0: win, 1: lose
#------------------------------------------------##
_userPlay:	
		addi	$sp, $sp, -4		# Make room for $ra on stack
		sw	$ra, 0($sp)		# Store $ra on stack
		add	$t0, $zero, $a0		# Copy arg to $t0 - addr of sequence
		add	$t9, $zero, $zero	# $t9 = 0	
 uP_lp:		lb	$t1, ($t0)		# Load byte from sequence block
 		beq	$t1, $zero, uP_win	# If we reach 0 in the sequence block, the user has won the round
		beq	$t9, $zero, uP_lp	# Loop for input (could have made a separate label but this is simpler)
		add	$t2, $zero $t9		# $t2 = input/guess
		bne	$t1, $t2, uP_lose	# $t1 != $t2, then they lose
 uP_wait:	bne	$t8, $zero, uP_wait	# Wait for previous tone to end if needed
		add	$t8, $zero, $t2		# If they guess correct, light up button
		add	$t9, $zero, $zero	# Ready for next input
		addi	$t0, $t0, 1		# Move to next code in sequence block
		j	uP_lp			# Loop through sequence block
 uP_win:	add	$v0, $zero, $zero	# $v0 = 0 if win
 		jr	$ra			# Return
 uP_lose:	bne	$t8, $zero, uP_lose	# Wait for previous tone to end if needed
		addi	$t8, $zero, 15		# Play game over tone
		jal	_clearBuffer		# Clears the sequence
		lw	$ra, 0($sp)		# Load $ra from stack
 		add	$sp, $sp, 4		# Move $sp back
 		jr	$ra			# Return
		
		
##-----------------------------------------------##
# _clearBuffer - clears sequence buffer
# Args:		$a0 - addr of sequence
# Returns:	none
#------------------------------------------------##
_clearBuffer:
		add	$t0, $zero, $a0		# Copy arg to $t0 = addr of sequence
		add	$t1, $zero, $zero	# Counter
 cB_lp:		beq	$t1, 100, cB_done	# If we've gove through 100 bytes, we're done
		sb	$zero, ($t0)		# Set($t0) to $zero
		addi	$t0, $t0, 1		# Increment current byte
		addi	$t1, $t1, 1		# Increment counter
		j	cB_lp			# Loop until we've set each byte to 0 in buffer
 cB_done:	jr	$ra			# Return
		
		 
		

		
		
		
