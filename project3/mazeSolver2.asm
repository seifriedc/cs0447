# Chris Seifried
# Project 3 - Maze Solver
# 11/13/2016
.data
	path:	.space	100	# For tracing back

.text	
		addi	$t8, $zero, 1		# Move forward initially
		la	$t3, path		# $t3 = end of the list
		add	$t0, $zero, 0x00FF	# Add for path 
		sh	$t0, ($t3)		# Add starting point to beginning of path
		addi	$t3, $t3, 2		# Adjust pointer	
		
main_lhr:	jal	_leftHandRule
		add	$t1, $zero, $v0		# Pointer to end of the list
		srl	$t0, $t9, 16		# Shift right 16 times
		beq	$t0, 0x0708, main_path	# Stop if we are done
		j	main_lhr		# Loop

main_path:	bne	$t8, $zero, main_path	# Wait if car is still moving
		addi	$t8, $zero, 3		# Rotate
rotate:		bne	$t8, $zero, rotate	# Wait if car is still moving
		addi	$t8, $zero, 3		# Rotate
		add	$a0, $zero, $t1		# Pass addr of end of list as arg
		jal	_traceBack		# Trace back path
		
main_bktrk:	bne	$t8, $zero, main_bktrk	# Wait if car is still moving
		addi	$t8, $zero, 3		# Rotate
rotate2:	bne	$t8, $zero, rotate2	# Wait if car is still moving
		addi	$t8, $zero, 3		# Rotate
move1:		bne	$t8, $zero, move1	# Wait if car is still moving
		addi	$t8, $zero, 1		# Move to [0,0]
		add	$a0, $zero, $zero	# $a0 = row
		add	$a1, $zero, $zero	# $a1 = col
		add	$a2, $zero, $zero	# $a2 = prev. row
		add	$a3, $zero, -1		# $a3 = prev. col
		jal	_backtracking		# Use backtracking to solve maze
		
		addi	$v0, $zero, 10
		syscall
		
		
#------------------------------------------------\
# _leftHandRule: Solve maze using left hand rule
# 	Return:		$v0 = addr of end of list
#------------------------------------------------/
_leftHandRule:	addi	$sp, $sp, -4		# Make room for $ra on stack
		sw	$ra, ($sp)		# Store $ra to stack
 LHR_w0:	bne	$t8, $zero, LHR_w0	# Wait for car to stop moving
		andi	$t0, $t9, 15		# Get status of car
		andi	$t1, $t0, 4		# Check status of left side of car
 		beq	$t1, 0, LHR_oL		# If no wall on left, turn left and move
 		andi	$t1, $t0, 8		# Check status of front of car
 		beq	$t1, 0, LHR_oF		# If no wall in front, move straight
 		andi	$t1, $t0, 12		# Check if walls in front and left
 		beq	$t1, 12, LHR_FL		# If walls in front and left, turn right
 		andi	$t1, $t0, 14		# Check if surrounded by walls
 		
 LHR_oL:	addi 	$t8, $zero, 2		# Turn left
 LHR_w1:	bne	$t8, $zero, LHR_w1	# Wait for car to stop moving
 		# Move forward - fall through

 LHR_oF:	add	$a0, $zero, $t2		# Add $t2 to $a0
  		add	$a1, $zero, $t3		# Add $t3 to $a1
  		addi	$sp, $sp, -16		# Adjust stack
  		sw	$t3, 12($sp)		# Push $t3 to stack
  		sw	$t2, 8($sp)		# Push $t2 to stack
 		sw	$t1, 4($sp)		# Push $t1 to stack
 		sw	$t0, 0($sp)		# Push $t0 to stack
  		jal 	_condensePath		# Condense path on the fly
  		lw	$t3, 12($sp)		# Pop $t3 from stack
  		lw	$t2, 8($sp)		# Pop $t2 from stack
 		lw	$t1, 4($sp)		# Pop $t1 from stack
 		lw	$t0, 0($sp)		# Pop $t0 from stack
 		add	$t2, $zero, $v0		# Set new positions counter for list
 	        addi	$sp, $sp, 16		# Adjust stack
  		
 		addi	$sp, $sp, -16		# Adjust stack
 		sw	$t3, 12($sp)		# Push $t3 to stack
 		sw	$t2, 8($sp)		# Push $t2 to stack
 		sw	$t1, 4($sp)		# Push $t1 to stack
 		sw	$t0, 0($sp)		# Push $t0 to stack
 		add	$a0, $zero, $t2		# Add $t2 to $a0
 		jal	_addPosToPath		# Add current position to path list
 		lw	$t3, 12($sp)		# Pop $t3 from stack
 		lw	$t2, 8($sp)		# Pop $t2 from stack
 		lw	$t1, 4($sp)		# Pop $t1 from stack
 		lw	$t0, 0($sp)		# Pop $t0 from stack
 	        addi	$sp, $sp, 16		# Adjust stack
 	        add	$t3, $zero, $v0		# $t3 = addr of end of list
 		
 		addi	$t2, $t2, 1		# $t2 = number of positions in path list. Increment
 		addi	$t8, $zero, 1		# Move straight
 		j LHR_ret			# Jump to return
 		
 LHR_FL:	addi	$t8, $zero, 3		# Turn right				
 		
 LHR_ret:	lw	$ra, ($sp)		# Load $ra from stack
 		addi 	$sp, $sp, 4		# Reset stack
 		add	$v0, $zero, $t3		# Return addr of end of the list
 		jr	$ra			# Return
 		
#-----------------------------------------------------\
# _addPosToPath: Add current position to path list
# 	Args:	    $a0 = number of positions in list
#	Return:	    $v0 = address of end of list
#-----------------------------------------------------/
_addPosToPath:	addi	$sp, $sp, -4		# Make room for $ra on stack
		sw	$ra, ($sp)		# Store $ra to stack
		
		add	$t0, $zero, $a0		# Copy arg to $t0
		sll	$t0, $t0, 1		# Multiply by 2 (for halfword alignment)
		la	$t1, path($t0)		# Load address of first available space in path to $t1
		srl	$t2, $t9, 16		# Shift right 16 times
		sh	$t2, ($t1)		# Store position in list
 		
 aPTP_ret:	lw	$ra, ($sp)		# Load $ra from stack
 		addi 	$sp, $sp, 4		# Reset stack
 		addi	$t1, $t1, 2		# Move pointer to very end of list
 		add	$v0, $zero, $t1		# Add to $v0 to return
 		jr	$ra			# Return

#---------------------------------------------------------\
# _condensePath: Make path list reflect the shortest path  
# 	Args:	    $a0 = number of positions in list
#		    $a1 = address of end of list
#	Return:     $v0 = new number of positions in list
#---------------------------------------------------------/
_condensePath:	addi	$sp, $sp, -4		# Make room for $ra on stack
		sw	$ra, ($sp)		# Store $ra on stack
		
		add     $t0, $zero, $a0		# Load arg into $t0 = number of positions in list
		add	$t1, $zero, $a1		# Load arg into $t1 = addr of end of list
		add	$t5, $zero, $zero	# $t5 = new number of positions in list
		
		srl	$t2, $t9, 16		# Shift right 16 times to access cur. pos.
		la	$t3, path		# Load path

 cP_lp:		lh	$t4, ($t3)		# Load next position in path list
		beq	$t2, $t4, cP_rm		# If duplicate position, delete everything after
		addi	$t3, $t3, 2		# Move pointer 2 bytes
		addi	$t5, $t5, 1		# Increment new number of positions
		bne	$t1, $t3, cP_lp		# If we haven't reached end of list, keep looping
		j	cP_ret			# We are done if we've reached the end of list	
		
 cP_rm:		sh	$zero, ($t3)		# Delete position entry at ($t3)
 		addi	$t3, $t3, 2		# Traverse list to next position entry
 		slt	$t6, $t1, $t3		# Check if we have gone through the whole list 
 		beq	$t6, 0, cP_rm		# Remove positions from list until we've reached end of list	
						
 cP_ret:	lw	$ra, ($sp)		# Load $ra from stack
		addi	$sp, $sp, 4		# Reset stack
		add 	$v0, $zero, $t5		# Put new number of positions in $v0
		jr 	$ra			# Return
		
		
#---------------------------------------------------------\
# _traceBack: Go back through maze using shortest path
# 	Args:	    $a0 = addr of end of the list
#---------------------------------------------------------/	
_traceBack: 	addi	$sp, $sp, -4		# Make room for $ra on stack
		sw	$ra, ($sp)		# Push $ra to stack

		add	$t0, $zero, $a0		# $t0 = pointer to end of the list
		sub	$t0, $t0, 2		# Move pointer initially
		
 tB_lp:		bne	$t8, $zero, tB_lp	# Wait for car to stop moving
 		srl	$t1, $t9, 16		# Shift right 16 times to access cur. pos.
 		beq	$t1, 0x00FF, tB_ret	# Return if we reached [0,-1]	
 		and	$t7, $t9, 3840		# Get which direction car is facing
		lh	$t2, ($t0)		# Load coordinate
		bne	$t2, 0x00FF, tB_cont 	# If next position is [0,-1], move left
		addi	$sp, $sp, -4		
		sw	$t0, ($sp)
		addi	$a0, $zero, 4
		jal	_move
		lw	$t0, ($sp)
		addi	$sp, $sp, 4
		j	tB_inc
		
 tB_cont:	
		# Look at row coordinate first
		and	$t3, $t1, 0xFF00	# $t3 = row coord of current pos
		and	$t4, $t2, 0xFF00	# $t4 = row coord of next pos
		slt	$t5, $t4, $t3		# $t5 = move up row if true
		slt	$t6, $t3, $t4		# $t6 = move down row if true
		beq	$t5, $t6, tB_chkC	# Skip the following if the row doesn't change
		
 tB_uR:		bne	$t5, 1, tB_dR		# If we aren't moving up, we're moving down
		bne	$t8, $zero, tB_uR	# Wait for car to stop moving
 		beq	$t7, 2048, tB_uRm	# If it is already facing north, move
		beq	$t7, 1024, tB_uRre	# If facing east, jump
 		addi	$t8, $zero, 3		# Facing west, rotate right to north
 		j	tB_uRm			# Jump to move north
 tB_uRre:	addi	$t8, $zero, 2		# Facing east, rotate left to north
 tB_uRm:	addi	$t8, $zero, 1		# Move forward
 		j 	tB_inc			# Loop
 		
 tB_dR:		bne	$t6, 1, tB_chkC 	# If we aren't moving up or down, we aren't moving vertically
 tB_dRr:	bne	$t8, $zero, tB_dR	# Wait for car to stop moving
 		beq	$t7, 512, tB_dRm	# If already facing down, move
 		beq	$t7, 1024, tB_dRre	# If facing east, jump to rotate
 		addi	$t8, $zero, 2		# Facing west, rotate left to south
 		j	tB_dRm			# Jump to move south
 tB_dRre:	addi	$t8, $zero, 3		# Facing east, rotate right to south
 tB_dRm:	addi	$t8, $zero, 1		# Move forward
 		j	tB_inc			# Loop
 		
 		# Look at column coordinate
 tB_chkC:	and	$t3, $t1, 0x00FF	# $t3 = col coord of current pos
		and	$t4, $t2, 0x00FF	# $t4 = col coord of next pos
		slt	$t5, $t3, $t4		# $t5 = move right if true
		slt	$t6, $t4, $t3		# $t6 = move left if true
		beq	$t5, $t6, tB_inc	# Loop if col doesn't change
		
 tB_rC:		bne	$t5, 1, tB_lC		# If we aren't moving right, we're moving left
		bne	$t8, $zero, tB_rC	# Wait for car to stop moving
 		beq	$t7, 1024, tB_rCm	# If it is already facing east, move
 		beq	$t7, 2048, tB_rCrn	# If facing north, jump to rotate
 		addi	$t8, $zero, 2		# Facing south, rotate left to east
 		j	tB_rCm			# Jump to move east
 tB_rCrn:	addi	$t8, $zero, 3		# Facing north, rotate right to east
 tB_rCm:	addi	$t8, $zero, 1		# Move forward
 		j 	tB_inc			# Loop
 		
 tB_lC:		bne	$t6, 1, tB_inc		# If we aren't moving left or right, we aren't moving horiz.
		bne	$t8, $zero, tB_lC	# Wait for car to stop moving
 		beq	$t7, 256, tB_lCm	# If already facing west, move
 		beq	$t7, 2048, tB_lCrn	# If facing north, jump to rotate
 		addi	$t8, $zero, 3		# Facing south, rotate right to west
 		j	tB_lCm			# Jump to move west
 tB_lCrn:	addi	$t8, $zero, 2		# Facing north, rotate left to west
 tB_lCm:	addi	$t8, $zero, 1		# Move
		
 tB_inc:	sub	$t0, $t0, 2		# Move pointer initially
 		j	tB_lp			# Loop		
 tB_ret:	lw	$ra, ($sp)		# Pop $ra from stack
 		addi	$sp, $sp, 4		# Readjust stack
 		jr	$ra			# Return

#---------------------------------------------------------\
# _backtracking: Solve using recursion/backtracking
#	Args:	$a0 = current row (r)
#		$a1 = current col (c)
#		$a2 = prev. row (pr)
#		$a3 = prev. col (pc)
#
#	Ret:	$v0 = boolean, move successful or not
#---------------------------------------------------------/	
_backtracking:	
		addi	$sp, $sp, -4		# Adjust stack
		sw	$ra, ($sp)		# Push $ra to stack
		add	$s0, $zero, $a0		# Copy row arg = $s0
		add	$s1, $zero, $a1		# Copy col arg = $s1
		add	$s2, $zero, $a2		# Copy prev row arg = $s2
		add	$s3, $zero, $a3		# Copy prev col arg = $s3
		bne	$s0, 7, _bt_recurse	# Check if r == 7
		beq	$s1, 8, _bt_ret_t	# If r == 7 and c == 8, return true
		
_bt_recurse:
		jal	_hasNorthWall		# Check if cur cell has north wall
		add	$s4, $zero, $v0		# Copy return value
		subi	$s5, $s0, 1		# Put r-1 into $s5
		beq	$s2, $s5, _bt_east	# if pr == r-1, skip to east check
		bne	$s4, 0, _bt_east	# if hasNorthWall == 0, recurse
		
		addi	$a0, $zero, 1		# Move north code
		jal	_move			# Move north
		
		addi	$sp, $sp, -28		# Adjust stack
		sw	$s6, 24($sp)
		sw	$s5, 20($sp)
		sw	$s4, 16($sp)
		sw	$s3, 12($sp)
		sw	$s2, 8($sp)
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)		
		add	$a0, $zero, $s5		# r = r-1
		add	$a1, $zero, $s1		# c = c
		add	$a2, $zero, $s0		# pr = r
		add	$a3, $zero, $s1		# pc = c
		jal	_backtracking	
		lw	$s6, 24($sp)
		lw	$s5, 20($sp)
		lw	$s4, 16($sp)
		lw	$s3, 12($sp)
		lw	$s2, 8($sp)
		lw	$s1, 4($sp)		
		lw	$s0, 0($sp)	
		addi	$sp, $sp, 28		# Adjust stack
		add	$s6, $zero, $v0		# Copy return value
		beq	$s6, 1, _bt_ret_t	# Return true if _backtracking returns true. Else...
		add	$a0, $zero, 3		# South code
		jal	_move			# Then move back	
		
 _bt_east:
 		jal	_hasEastWall		# Check if cur cell has east wall
		add	$s4, $zero, $v0		# Copy return value
		addi	$s5, $s1, 1		# Put c+1 into $s5
		beq	$s3, $s5, _bt_south	# if pc == c+1, skip to south check
		bne	$s4, 0, _bt_south	# if hasEastWall == 0, recurse
		
		addi	$a0, $zero, 2		# Move east code
		jal	_move			# Move east
		
		addi	$sp, $sp, -28		# Adjust stack
		sw	$s6, 24($sp)
		sw	$s5, 20($sp)
		sw	$s4, 16($sp)
		sw	$s3, 12($sp)
		sw	$s2, 8($sp)
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)		
		add	$a0, $zero, $s0		# r = r
		add	$a1, $zero, $s5		# c = c+1
		add	$a2, $zero, $s0		# pr = r
		add	$a3, $zero, $s1		# pc = c
		jal	_backtracking
		lw	$s6, 24($sp)	
		lw	$s5, 20($sp)
		lw	$s4, 16($sp)
		lw	$s3, 12($sp)
		lw	$s2, 8($sp)
		lw	$s1, 4($sp)		
		lw	$s0, 0($sp)	
		addi	$sp, $sp, 28		# Adjust stack
		add	$s6, $zero, $v0		# Copy return value
		beq	$s6, 1, _bt_ret_t	# Return true if _backtracking returns true. Else...
		add	$a0, $zero, 4		# West code
		jal	_move			# Then move back
			
 _bt_south:
 		jal	_hasSouthWall		# Check if cur cell has south wall
		add	$s4, $zero, $v0		# Copy return value
		addi	$s5, $s0, 1		# Put r+1 into $s5
		beq	$s2, $s5, _bt_west	# if pr == r+1, skip to west check
		bne	$s4, 0, _bt_west	# if hasSouthWall == 0, recurse
		
		addi	$a0, $zero, 3		# Move south code
		jal	_move			# Move south
		
		addi	$sp, $sp, -28		# Adjust stack
		sw	$s6, 24($sp)
		sw	$s5, 20($sp)
		sw	$s4, 16($sp)
		sw	$s3, 12($sp)
		sw	$s2, 8($sp)
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)		
		add	$a0, $zero, $s5		# r = r+1
		add	$a1, $zero, $s1		# c = c
		add	$a2, $zero, $s0		# pr = r
		add	$a3, $zero, $s1		# pc = c
		jal	_backtracking	
		lw	$s6, 24($sp)
		lw	$s5, 20($sp)
		lw	$s4, 16($sp)
		lw	$s3, 12($sp)
		lw	$s2, 8($sp)
		lw	$s1, 4($sp)		
		lw	$s0, 0($sp)	
		addi	$sp, $sp, 28		# Adjust stack
		add	$s6, $zero, $v0		# Copy return value
		beq	$s6, 1, _bt_ret_t	# Return true if _backtracking returns true. Else...
		add	$a0, $zero, 1		# North code
		jal	_move			# Then move back

 
 _bt_west:		
		jal	_hasWestWall		# Check if cur cell has west wall
		add	$s4, $zero, $v0		# Copy return value
		subi	$s5, $s1, 1		# Put c-1 into $s5
		beq	$s3, $s5, _bt_ret_f	# if pc == c-1, return false
		bne	$s4, 0, _bt_ret_f	# if hasWestWall == 0, recurse
		
		addi	$a0, $zero, 4		# Move west code
		jal	_move			# Move west
		
		addi	$sp, $sp, -28		# Adjust stack
		sw	$s6, 24($sp)	
		sw	$s5, 20($sp)
		sw	$s4, 16($sp)
		sw	$s3, 12($sp)
		sw	$s2, 8($sp)
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)		
		add	$a0, $zero, $s0		# r = r
		add	$a1, $zero, $s5		# c = c-1
		add	$a2, $zero, $s0		# pr = r
		add	$a3, $zero, $s1		# pc = c
		jal	_backtracking	
		lw	$s6, 24($sp)
		lw	$s5, 20($sp)
		lw	$s4, 16($sp)
		lw	$s3, 12($sp)
		lw	$s2, 8($sp)
		lw	$s1, 4($sp)		
		lw	$s0, 0($sp)	
		addi	$sp, $sp, 28		# Adjust stack
		add	$s6, $zero, $v0		# Copy return value
		beq	$s6, 1, _bt_ret_t	# Return true if _backtracking returns true. Else...
		add	$a0, $zero, 2		# East code
		jal	_move			# Then move back
		
		j	_bt_ret_f		# Return false
		
		
_bt_ret_t:	addi	$v0, $zero, 1		# Return true
		j	_bt_ret
_bt_ret_f:	add	$v0, $zero, $zero	# Return false
_bt_ret:	lw	$ra, ($sp)		# Load $ra from stack
		addi	$sp, $sp, 4		# Adjust stack back
		jr	$ra			# Return

#--------------------------------------------------------------\
# _hasNorthWall: Detects north wall independent of orientation
#	Return:		$v0 = 0 or 1
#--------------------------------------------------------------/	
_hasNorthWall:	
		addi	$sp, $sp, -8		# Adjust stack
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)	
		
		and	$s0, $t9, 3840		# Get which direction car is facing
		beq	$s0, 2048, _hNW_fn	# If facing north, handle
		beq	$s0, 1024, _hNW_fe	# If facing east, handle
		beq	$s0, 512, _hNW_fs	# If facing south, handle
		beq	$s0, 256, _hNW_fw	# If facing west, handle
 _hNW_fn:	and	$s1, $t9, 8		# Check front wall
 		srl	$v0, $s1, 3		# Shift for return
 		j	_hNW_ret		# Return
 _hNW_fe:	and	$s1, $t9, 4		# Check left wall
 		srl	$v0, $s1, 2		# Shift for return
 		j	_hNW_ret		# Return
 _hNW_fw:	and	$s1, $t9, 2		# Check right wall
 		srl	$v0, $s1, 1		# Shift for return
 		j	_hNW_ret		# Return
 _hNW_fs:	and	$v0, $t9, 1		# Check back wall
 
 _hNW_ret:	lw	$s1, 4($sp)		
		lw	$s0, 0($sp)
 		add	$sp, $sp, 8
 		jr	$ra
	
#--------------------------------------------------------------\
# _hasEastWall: Detects east wall independent of orientation
#	Return:		$v0 = 0 or 1
#--------------------------------------------------------------/	
_hasEastWall:	
		addi	$sp, $sp, -8		# Adjust stack
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)	
		and	$s0, $t9, 3840		# Get which direction car is facing
		beq	$s0, 2048, _hEW_fn	# If facing north, handle
		beq	$s0, 1024, _hEW_fe	# If facing east, handle
		beq	$s0, 512, _hEW_fs	# If facing south, handle
		beq	$s0, 256, _hEW_fw	# If facing west, handle
 _hEW_fe:	and	$s1, $t9, 8		# Check front wall
 		srl	$v0, $s1, 3		# Shift for return
 		j	_hEW_ret		# Return
 _hEW_fs:	and	$s1, $t9, 4		# Check left wall
  		srl	$v0, $s1, 2		# Shift for return
 		j	_hEW_ret		# Return
 _hEW_fn:	and	$s1, $t9, 2		# Check right wall
 		srl	$v0, $s1, 1		# Shift for return
 		j	_hEW_ret		# Return
 _hEW_fw:	and	$v0, $t9, 1		# Check back wall
 
 _hEW_ret:	lw	$s1, 4($sp)		
		lw	$s0, 0($sp)	
		addi	$sp, $sp, 8		# Adjust stack
 		jr	$ra

#--------------------------------------------------------------\
# _hasSouthWall: Detects south wall independent of orientation
#	Return:		$v0 = 0 or 1
#--------------------------------------------------------------/	
_hasSouthWall:	
		addi	$sp, $sp, -8		# Adjust stack
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)
		and	$s0, $t9, 3840		# Get which direction car is facing
		beq	$s0, 2048, _hSW_fn	# If facing north, handle
		beq	$s0, 1024, _hSW_fe	# If facing east, handle
		beq	$s0, 512, _hSW_fs	# If facing south, handle
		beq	$s0, 256, _hSW_fw	# If facing west, handle
 _hSW_fs:	and	$s1, $t9, 8		# Check front wall
 		srl	$v0, $s1, 3		# Shift for return
 		j	_hSW_ret		# Return
 _hSW_fw:	and	$s1, $t9, 4		# Check left wall
  		srl	$v0, $s1, 2		# Shift for return
 		j	_hSW_ret		# Return
 _hSW_fe:	and	$s1, $t9, 2		# Check right wall
 		srl	$v0, $s1, 1		# Shift for return
 		j	_hSW_ret		# Return
 _hSW_fn:	and	$v0, $t9, 1		# Check back wall
 
 _hSW_ret:	lw	$s1, 4($sp)		
		lw	$s0, 0($sp)
		addi	$sp, $sp, 8		# Adjust stack
 		jr	$ra				
			
#--------------------------------------------------------------\
# _hasWestWall: Detects west wall independent of orientation
#	Return:		$v0 = 0 or 1
#--------------------------------------------------------------/	
_hasWestWall:	addi	$sp, $sp, -8		# Adjust stack
		sw	$s1, 4($sp)		
		sw	$s0, 0($sp)
		and	$s0, $t9, 3840		# Get which direction car is facing
		beq	$s0, 2048, _hWW_fn	# If facing north, handle
		beq	$s0, 1024, _hWW_fe	# If facing east, handle
		beq	$s0, 512, _hWW_fs	# If facing south, handle
		beq	$s0, 256, _hWW_fw	# If facing west, handle
 _hWW_fw:	and	$s1, $t9, 8		# Check front wall
 		srl	$v0, $s1, 3		# Shift for return
 		j	_hWW_ret		# Return
 _hWW_fn:	and	$s1, $t9, 4		# Check left wall
  		srl	$v0, $s1, 2		# Shift for return
 		j	_hWW_ret		# Return
 _hWW_fs:	and	$s1, $t9, 2		# Check right wall
 		srl	$v0, $s1, 1		# Shift for return
 		j	_hWW_ret		# Return
 _hWW_fe:	and	$v0, $t9, 1		# Check back wall
 
 _hWW_ret:	lw	$s1, 4($sp)		
		lw	$s0, 0($sp)
		addi	$sp, $sp, 8
 		jr	$ra				
				
#-------------------------------------------------------------\
# _move: Rotate and move car correctly based on orientation
#	Args:	$a0 => n = 1, e = 2, s = 3, w = 4
#-------------------------------------------------------------/
_move:		
		add	$t0, $zero, $a0		# $t0 = direction to move
 _move_wait:	bne	$t8, $zero, _move_wait	# Wait for car to stop moving
		and	$t1, $t9, 3840		# Get which direction car is facing
		beq	$t0, 1, _m_n		# Move north
		beq	$t0, 2, _m_e		# Move east
		beq	$t0, 3, _m_s		# Move south
		beq	$t0, 4, _m_w		# Move west
 
 _m_n:		beq	$t1, 2048, _m_n_u	# If already facing up
 		beq	$t1, 1024, _m_n_r	# If facing right
 		beq	$t1, 512, _m_n_d	# If facing down
 		beq	$t1, 256, _m_n_l	# If facing left
 		
  _m_n_u:	j	_move_ret		# Already facing north so move 			
  _m_n_r:	j	_m_tl			# Facing east, turn left
  _m_n_d:	j	_m_tl2			# Facing down, turn left x2
  _m_n_l:	j	_m_tr			# Facing west, turn right
  
 _m_e:		beq	$t1, 2048, _m_e_u	# If facing up
 		beq	$t1, 1024, _m_e_r	# If already facing right
 		beq	$t1, 512, _m_e_d	# If facing down
 		beq	$t1, 256, _m_e_l	# If facing left
 		
  _m_e_r:	j	_move_ret		# Already facing east so move 			
  _m_e_d:	j	_m_tl			# Facing down, turn left
  _m_e_l:	j	_m_tl2			# Facing left, turn left x2
  _m_e_u:	j	_m_tr			# Facing up, turn right	
 
 
 _m_s:		beq	$t1, 2048, _m_s_u	# If facing up
 		beq	$t1, 1024, _m_s_r	# If facing right
 		beq	$t1, 512, _m_s_d	# If already facing down
 		beq	$t1, 256, _m_s_l	# If facing left
 		
  _m_s_d:	j	_move_ret		# Already facing down so move 			
  _m_s_l:	j	_m_tl			# Facing left, turn left
  _m_s_u:	j	_m_tr2			# Facing up, turn right x2
  _m_s_r:	j	_m_tr			# Facing right, turn right	
 
 _m_w:		beq	$t1, 2048, _m_w_u	# If facing up
 		beq	$t1, 1024, _m_w_r	# If facing right
 		beq	$t1, 512, _m_w_d	# If facing down
 		beq	$t1, 256, _m_w_l	# If already facing left
 		
  _m_w_l:	j	_move_ret		# Already facing left so move 			
  _m_w_u:	j	_m_tl			# Facing up, turn left
  _m_w_r:	j	_m_tr2			# Facing right, turn right x2
  _m_w_d:	j	_m_tr			# Facing down, turn right
 
 
 _m_tl:		addi	$t8, $zero, 2		# Turn left
 		j	_move_ret		# Return
 _m_tr:		addi	$t8, $zero, 3		# Turn right
 		j	_move_ret		# Return
 _m_tl2:	addi	$t8, $zero, 2		# Turn left x2
  _mw1:		bne	$t8, $zero, _mw1	# Wait for car to stop turning
  		addi	$t8, $zero, 2		# Turn left again
  		j	_move_ret		# Return
 _m_tr2:	addi	$t8, $zero, 3		# Turn right x2
  _mw2:		bne	$t8, $zero, _mw2	# Wait for car to stop turning
  		addi	$t8, $zero, 3		# Turn right again
  		#j	_move_ret		# Return
  		
 _move_ret:	bne	$t8, $zero, _move_ret	# Wait for car to stop moving
 		addi	$t8, $zero, 1		# Move forward
 		jr	$ra
	