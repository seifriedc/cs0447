#===| Chris Seifried                |===#
#===| Lab02_C: Rock, Paper Scissors |===#
#===| September 17, 2016            |===#

# Note: input only works with capital letters

.data
	makeMoveTxt:	.asciiz "Make a move (R, P, S, Q): "
	
	youMadeTxt:	.asciiz "\nYou made "
	cpuMadeTxt:	.asciiz ", Computer made "
	periodTxt:	.asciiz "."
	
	rockTxt:	.asciiz "Rock"
	paperTxt:	.asciiz "Paper"
	scissorsTxt:	.asciiz "Scissors"
	
	winTxt:		.asciiz "\nYou Won Congratulations\n\n"
	loseTxt:	.asciiz "\nYou lost, Good luck next time\n\n"
	tieTxt:		.asciiz "\nTie\n\n"
	
.text
	# $t0 = user selection
	# $t1 = cpu selection, int
	# $s0 = system time, least significant part
	
	#-- Moves code convention --#
	# 0 = ROCK
	# 1 = PAPER
	# 2 = SCISSORS

	gameLoop:
	#--- Print prompt ---#
		addi	$v0, $zero, 4
		la	$a0, makeMoveTxt
		syscall
	#--- Read input from user ---#
		addi 	$v0, $zero, 12
		syscall
		add 	$t0, $zero, $v0
	#--- Check if quit ---#
		beq 	$t0, 81, quit
	#--- Generate random number ---#
		# Get system time -> $s0
		addi 	$v0, $zero, 30
		syscall
		add  	$s0, $zero, $a0
		
		# Set RNG seed
		addi 	$v0, $zero, 40
		addi 	$a0, $zero, 1 		# RNG ID
		add  	$a1, $zero, $s0
		syscall
		
		# Get random int -> $t1
		addi 	$v0, $zero, 42
		addi	$a0, $zero, 1
		addi  	$a1, $zero, 3
		syscall
		add	$t1, $zero, $a0
	#--- Output "You made " ---#
		addi	$v0, $zero, 4
		la	$a0, youMadeTxt
		syscall
	#--- Parse user input, branch based on selection ---#
		beq	$t0, 82, rock
		beq	$t0, 80, paper
		beq	$t0, 83, scissors
		
	#--- Display user choice, set $t0 to 0,1,2 (R,P,S), and branch ---#
	rock:
		la	$a0, rockTxt
		syscall
		
		addi	$t0, $zero, 0
		
		beq	$t1, 0, cpuRock
		beq	$t1, 1, cpuPaper
		beq	$t1, 2, cpuScissors
		
	paper:
		la	$a0, paperTxt
		syscall
		
		addi	$t0, $zero, 1
		
		beq	$t1, 0, cpuRock
		beq	$t1, 1, cpuPaper
		beq 	$t1, 2, cpuScissors
	
	scissors:
		la	$a0, scissorsTxt
		syscall
		
		addi	$t0, $zero, 2
		
		beq	$t1, 0, cpuRock
		beq	$t1, 1, cpuPaper
		beq 	$t1, 2, cpuScissors
		
	#--- Diplay cpu choice, and do core game logic ---#
	cpuRock:
		la	$a0, cpuMadeTxt
		syscall
		la	$a0, rockTxt
		syscall
		la	$a0, periodTxt
		
		beq	$t0, 0, tie
		beq	$t0, 1, win
		beq	$t0, 2, lose
		
	cpuPaper:
		la	$a0, cpuMadeTxt
		syscall
		la	$a0, paperTxt
		syscall
		la	$a0, periodTxt
	
		beq	$t0, 0, lose
		beq	$t0, 1, tie
		beq	$t0, 2, win
	
	cpuScissors:
		la	$a0, cpuMadeTxt
		syscall
		la	$a0, scissorsTxt
		syscall
		la	$a0, periodTxt
		
		beq	$t0, 0, win
		beq	$t0, 1, lose
		beq	$t0, 2, tie
		
	#--- Dsiplay win/loss/tie text and jump to top of loop ---#	
	win:
		la	$a0, winTxt
		syscall
		j gameLoop
		
	lose:
		la	$a0, loseTxt
		syscall
		j gameLoop
		
	tie:
		la	$a0, tieTxt
		syscall
		j gameLoop
	
	quit:
		addi 	$v0, $zero, 10
		syscall
