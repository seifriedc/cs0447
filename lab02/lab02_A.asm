.data
	firstPrompt:	.asciiz "What is the first value?\n"
	secondPrompt:	.asciiz "What is the second value?\n"
	msgSum:		.asciiz "The sum of "
	msgAnd:		.asciiz " and "
	msgIs:		.asciiz " is "
	newline:	.asciiz "\n"
	
.text
	## Ask for first value
	addi 	$v0, $zero, 4
	la	$a0, firstPrompt
	syscall
	# Retrieve first value from user
	addi	$v0, $zero, 5
	syscall
	add	$t0, $zero, $v0
	# newline
	addi	$v0, $zero, 4
	la	$a0, newline
	syscall
	
	## Ask for second value
	addi 	$v0, $zero, 4
	la	$a0, secondPrompt
	syscall
	# Retrieve second value from user
	addi	$v0, $zero, 5
	syscall
	add	$t1, $zero, $v0
	# newline
	addi	$v0, $zero, 4
	la	$a0, newline
	syscall
	
	## Add the numbers
	add	$t2, $t0, $t1
	
	## Print output message
	la	$a0, msgSum
	syscall
	addi	$v0, $zero, 1
	add	$a0, $zero, $t0
	syscall
	addi	$v0, $zero, 4
	la	$a0, msgAnd
	syscall
	addi	$v0, $zero, 1
	add	$a0, $zero, $t1
	syscall
	addi	$v0, $zero, 4
	la	$a0, msgIs
	syscall
	addi	$v0, $zero, 1
	add	$a0, $zero, $t2
	syscall
	
	# Exit
	addi	$v0, $zero, 10
	syscall
