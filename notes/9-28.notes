-----------------------------------------------------------
    Functions
-----------------------------------------------------------

Procedure call:
- Use "jal myFunction" 
	- Jump and link
	- Will update $ra to $pc + 4, aka the instruction right after jal call
	- Performs to things
		1. Updates $ra
		2. Jumps

Procedure return:
- Use "jr $ra"
	- Sets PC to the value stored in $ra
	- So $ra is destination to Jump

Arguments & Return values:
- Register convemtions
	- $a0-$a3: four arguments for passing values to the called procedure
	- $v0 and $v1: two values returned from called procedure
	- $ra: return address register
- Sending arguments
	- Caller stores argument values in registers $a0-$a3
	- Called procedure will know that argument values are stored in those registers
- Returning values
	- Called procedure stores return values in registers $v0 and $v1
	- Caller will know that return values are stored in those registers
- Don't use v0-v1, a0-a3 as working registers
- # First copy into temp registers # 

Call chain:
	- $ra gets overwritten/destroyed when calling a function from a function
	- Set v0 to result/return value
	- Careful when using temp registers, make sure they aren't overwritten
		by calling another function
	- <caller> calls other procedures, <callee> is called by other procedures
	- Caller may want to use same registers as callee, and vice-versa
	- $t0-$t9
		- Caller should not expect that values will not be destroyed by callee
		- Assume that these will be destroyed by callee
		- Caller must back up $tX somewhere, restore after function call
	- $s0-$s7
		- Temporary saved registers
		- Callee MUST maintain their values
		- If callee wants to use these, callee must save their values before using them
		- Back up $sX before using, then restore after function call

Where to save values of registers?:
	- Main memory - stack
	- Load word (lw) and store word (sw)