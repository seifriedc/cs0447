# Chris Seifried
# Lab 4 - Function Calls
# 10/3/2016

main:		
.data
	prompt: .asciiz "Enter a string:\n"
	chars1: .asciiz "This string has "
	chars2: .asciiz " characters.\n"
	startI: .asciiz "Specify start index: "
	endI:	.asciiz "\nSpecify end index: "
	substr: .asciiz "Your substring is:\n"

	buffer: 	.space	64
	outBuffer:	.space	64

.text	
		addi	$v0, $zero, 4		# Add 4 to $v0 to print string
		la	$a0, prompt		# Load prompt
		syscall				# Print prompt
		
		jal	_readString		# Call _readString
		
		addi	$v0, $zero, 4		# Add 4 to $v0 to print string
		la	$a0, startI		# Ask for start index
		syscall
		
		addi	$v0, $zero, 5		# Read start index in
		syscall
		add	$a2, $zero, $v0		# Copy input to $a2 
		
		addi	$v0, $zero, 4		# Add 4 to $v0 to print string
		la	$a0, startI		# Ask for end index
		syscall
		
		addi	$v0, $zero, 5		# Read end index in
		syscall
		add	$a3, $zero, $v0		# Copy input to $a3 
		
		la	$a0, buffer		# Load input buffer
		la	$a1, outBuffer		# Load output buffer
		
		jal	_subString		# Call _subString
		
		addi	$v0, $zero, 4		# Add 4 to $v0 to print string
		la	$a0, substr		# Ask for end index
		syscall
		
		addi	$v0, $zero, 4		# Add 4 to $v0 to print string
		la	$a0, outBuffer		# Ask for end index
		syscall
		
		
		addi	$v0, $zero, 10		# Exit safely
		syscall
		
##-----------------------------------------------##
# _readString - read null-terminated string 
#		into memory from user input
#
# Args:		$a0 - address of an input buffer
#		$a1 - max number of chars to read in
#
# Returns:	None
#------------------------------------------------##
_readString:
.text
		add	$t9, $zero, $ra		# Save the register we have to jump back to
		add	$v0, $zero, 8		# Add to $v0 to get input string
		la	$a0, buffer		# Load addr of buffer to $a0 to store input there
		addi	$a1, $zero, 64		# Specify max size of input, store in $a1 for syscall
		syscall				# Get input
		jal	_strLength
		add	$t0, $zero, $v0		# Copy return value from _strLength to $t0
		addi	$t0, $t0, -1		# Offset to '\n' character, store index number
		add	$s0, $zero, $t0		# Length of string -> $s0 for later
		add	$t1, $t0, $a0		# Address of the byte we want to change	in $t1	
		sb	$zero, ($t1) 		# Set the byte at ($t1) to zero (NULL character \0)
		
		# Print info
		add 	$v0, $zero, 4		# Print number of characters in string
		la	$a0, chars1		# Load text
		syscall
		add 	$v0, $zero, 1		# Print number
		add	$a0, $zero, $t0		# Load number to arg for syscall
		syscall
		add	$v0, $zero, 4		# Print text
		la	$a0, chars2
		syscall		
		
		add	$ra, $zero, $t9		# Set the return address back
		jr	$ra			# Return


##------------------------------------------##
# _strLength - get length of a string
#
# Args:		$a0 - address of string
# Returns:	$v0 - length of the string
#-------------------------------------------##
_strLength:
.text
		add	$t1, $zero, $zero	# $t1 = counter. Return this value later
		add	$t2, $zero, $zero	# $t2 = current byte
lp_strLength:	add	$t0, $a0, $t1		# Add offset to current address, store in $t0 		
		lbu	$t2, ($t0)		# Load current byte into $t2.
		beq	$t2, 0, ret_strLength	# Branch if we've hit the null character
		addi	$t1, $t1, 1		# Add 1 to counter
		j	lp_strLength		# Jump back up to top of loop, keep counting
ret_strLength:	add	$v0, $zero, $t1		# Set return value $t1 to $v0
		jr	$ra			# Return
		


##-----------------------------------------------------------------##
# _subString - gets a substring of a string
#
# Args:		$a0 - address of an input string
#		$a1 - address of an output buffer
#		$a2 - start index for the input string (incl)
#		$a3 - end index for the input string (excl)
#	
# Returns:	$v0 - address of the output buffer
#------------------------------------------------------------------##		
_subString:
.text		
		add	$t9, $zero, $ra		# Save the register we have to jump back to
		add	$t0, $zero, $a0		# $t0 = input str address
		add	$t1, $zero, $a1		# $t1 = output str address
		add	$t2, $zero, $a2		# $t2 = start index
		add	$t3, $zero, $a3		# $t3 = end index
		add	$t0, $t0, $t2		# Set string address with offset
		
		slt	$t4, $s0, $t3		# Is the start index greater than length of str?
		bne	$t4, 1, start		# If not, enter loop
		add	$t3, $zero, $s0		# If true, set end index to length of string

start:		slt	$t4, $t2, $zero		# If either index < 0...
		slt	$t5, $t3, $zero		
		or	$t6, $t4, $t5		
		slt	$t4, $t3, $t2		# or if end < start...
		or	$t7, $t4, $t6		
		bne	$t7, 1, lp_subString	# is true...
		sb	$zero, ($t1)		# Then set output buffer to \0
		j	ret_subString
		
		
lp_subString:	beq	$t2, $t3, ret_subString	# If start = end, we're done looping
		lb	$t7, ($t0)		# Load byte into temp register $t7
		sb	$t7, ($t1)		# Copy input str to output at current index
		addi	$t0, $t0, 1		# Increment input addr index
		addi	$t1, $t1, 1		# Increment output addr index
		addi	$t2, $t2, 1		# Increment start index
		j	lp_subString		# Loop
		
ret_subString:	sb	$zero, ($t1)				
		add	$v0, $zero, $t1		# To return
		add	$ra, $zero, $t9		# Restore return address
		jr	$ra			# Return 
