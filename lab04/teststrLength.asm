main:		
.data
	prompt: .asciiz "Enter a string:\n"
	buffer: .space	64

.text	
		addi	$v0, $zero, 4		# Add 4 to $v0 to print string
		la	$a0, prompt		# Load prompt
		syscall				# Print prompt
		
		addi	$v0, $zero, 8
		la	$a0, buffer
		addi	$a1, $zero, 64
		syscall
		
		jal _strLength
		
		add	$a0, $zero, $v0
		addi	$v0, $zero, 1
		syscall
		
		addi	$v0, $zero, 10
		syscall
		

##------------------------------------------##
# _strLength - get length of a string
#
# Args:		$a0 - address of string
# Returns:	$v0 - length of the string
#-------------------------------------------##
_strLength:
.text
		add	$t1, $zero, $zero	# $t1 = counter. Return this value later
		add	$t2, $zero, $zero	# $t2 = current byte
lp_strLength:	add	$t0, $a0, $t1		# Add offset to current address, store in $t0 		
		lbu	$t2, ($t0)		# Load current byte into $t2.
		beq	$t2, 0, ret_strLength	# Branch if we've hit the null character
		addi	$t1, $t1, 1		# Add 1 to counter
		j	lp_strLength		# Jump back up to top of loop, keep counting
ret_strLength:	add	$v0, $zero, $t1		# Set return value $t1 to $v0
		jr	$ra			# Return
		
