## Chris Seifried
## Lab 3 Part 1
.data
	prompt: 	.asciiz "Please enter your integer: "
	output:		.asciiz "Here is the output: "
.text
	# Display prompt
	addi	$v0, $zero, 4
	la	$a0, prompt
	syscall
	
	# Get integer
	addi	$v0, $zero, 5
	syscall
	add	$t0, $zero, $v0
	
	# Manipulate bits
	srl	$t1, $t0, 15
	andi	$t1, $t1, 7
	
	# Display output
	addi	$v0, $zero, 4
	la	$a0, output
	syscall
	addi	$v0, $zero, 1
	add	$a0, $zero, $t1
	syscall
	
	# Exit
	addi	$v0, $zero, 10
	syscall
	
	
