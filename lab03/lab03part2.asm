## Chris Seifried
## Lab 3 Part 2
.data
	title:		.asciiz "x*y calculator\n"
	promptX:	.asciiz "Please enter x: "
	promptY:	.asciiz "Please enter y: "
	invalid:	.asciiz "Integer must be nonnegative.\n"
	star:		.asciiz "*"
	equals:		.asciiz " = "
	
.text
		# $t0 - result
		# $t1 - x
		# $t2 - y
		# $t3 - Copy of $t2 before calculation
		# $s1 - shifting counter
			
		# Display title
		addi	$v0, $zero, 4
		la	$a0, title
		syscall
		
		# Ask for inputs
inputLoopX:	addi	$v0, $zero, 4		# Set $v0 to 4
		la	$a0, promptX		# Load promptX string
		syscall				# Print string
		addi	$v0, $zero, 5		# Set $v0 to 5
		syscall				# Read integer
		add	$t1, $zero, $v0 	# Copy input to $t1
		slt	$s0, $t1, $zero		# Compare if input is negative
		beq	$s0, 0, inputLoopY	# If input good make jump
		
		addi	$v0, $zero, 4		# Set $v0 to 4
		la	$a0, invalid		# Load invalid string
		syscall				# Print string
		j	inputLoopX		# Jump back to top of input loop

inputLoopY:	addi	$v0, $zero, 4		# Set $v0 to 4
		la	$a0, promptY		# Load promptY string
		syscall				# Print string
		addi	$v0, $zero, 5		# Set $v0 to 5
		syscall				# Read integer
		add	$t2, $zero, $v0 	# Copy input to $t2
		add	$t3, $zero, $t2		# Copy y to $t3
		slt	$s0, $t2, $zero		# Compare if input is negative
		beq	$s0, 0, calculate	# If input good make jump
		
		addi	$v0, $zero, 4		# Set $v0 to 4
		la	$a0, invalid		# Load invalid string
		syscall				# Print string
		j	inputLoopY		# Jump back to top of input loop

		# Perform calculation w/ shifting and addition
calculate:	beq	$t2, 0, output		# While Y!=0
		andi	$s0, $t2, 1		# Checking first bit of y, $s0 is temp
		beq	$s0, 0, shiftY		# If first bit of y == 0 skip ahead
		sllv	$s2, $t1, $s1		# Shift x left $s1 times
		add	$t0, $t0, $s2		# Add $s2 to result
		
shiftY:		add	$s1, $s1, 1		# Add 1 to shift counter
		srl	$t2, $t2, 1		# Right shift $t2 by 1
		j calculate			# Jump back up to calculate; loop
		
		# Output the result
output:		addi	$v0, $zero, 1		# Set $v0 to 1
		add	$a0, $zero, $t1		# Set $a0 to $t1 for printing
		syscall				# Print $t1 (x)
		addi	$v0, $zero, 4		# Set $v0 to 4
		la 	$a0, star		# Load star
		syscall				# Print star
		addi	$v0, $zero, 1		# Set $v0 to 1
		add	$a0, $zero, $t3		# Set $a0 to $t3 for printing
		syscall				# Print $t3 (y)
		addi	$v0, $zero, 4		# Set $v0 to 4
		la 	$a0, equals		# Load equal sign
		syscall				# Print equal sign
		addi	$v0, $zero, 1		# Set $v0 to 1
		add	$a0, $zero, $t0		# Set $a0 to $t3 for printing
		syscall				# Print $t0 (result)
		
		addi	$v0, $zero, 10		# Exit program
		syscall
