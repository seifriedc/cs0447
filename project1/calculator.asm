.text
## Chris Seifried
## Project 1 - Calculator
## 10/12/2016

### Conventions ###
# $t5 = most recent input code
# $s0 = first operand
# $s1 = second operand
# $s2 = operator code
# $s3 = result

# Code is organized by state (as specified in assignment), and then by the type of input (number, operator, etc.)

#### State 0 ####
state0:		add	$t0, $zero, $zero	# Clear temp
		add	$t1, $zero, $zero	# Clear temp
		add	$t2, $zero, $zero	# Clear temp
		add	$t3, $zero, $zero	# Clear temp
		add	$t4, $zero, $zero	# Clear temp
		add	$t5, $zero, $zero	# Clear temp
		add	$s0, $zero, $zero	# Clear operand 1
		add	$s1, $zero, $zero	# Clear operand 2
		add	$s2, $zero, $zero	# Clear operator
		add	$t8, $zero, $zero	# Clear output
		add	$t9, $zero, $zero	# Clear input
		add	$t8, $zero, $s0		# Display input
		j	state1			# Jump to state 1
		
#### State 1 ####
state1:		beq	$t9, $zero, state1	# Keep waiting for input
		andi	$t5, $t9, 15		# AND input with 15, set into $t5
		add	$t9, $zero, $zero	# Reset $t9 back to 0 after we're done parsing input
		slti	$t1, $t5, 14		# If input is less than 14, set $t1 to true
		beq	$t1, 0,	st1_clrEq	# If input >= 14, go to st1_clrEq (pressed clear or equals)
		slti	$t1, $t5, 10		# If input code is < 10, set $t1 to true
		beq	$t1, 0, st1_op		# If input code >= 10, go to st1_op
st1_number:	sll	$t2, $s0, 3		# ($s0 << 3), store in $t2
		sll	$t3, $s0, 1		# ($s0 << 1), store in $t3
		add	$s0, $t2, $t3 		# Add ($s0 << 3) + ($s0 << 1)
		add	$s0, $s0, $t5		# Add input to $s0
		add	$t8, $zero, $s0		# Display input
		j	state1			# Anticipate another input
st1_op:		add	$s2, $zero, $t5		# Copy operator code to $s2
		add	$t8, $zero, $s0		# Display operand 1
		j	state2			# Go to state 2
st1_clrEq:	beq	$t5, 14, st1_equals	# If input is 15, go to clear
		j	state0			# Jump to state0
st1_equals:	add	$s3, $zero, $s0		# Set result as operand1
		add	$t8, $zero, $s3		# Display result
		j	state4			# Jump to state4

#### State 2 ####
state2:		beq	$t9, $zero, state2	# Keep waiting for input
		andi	$t5, $t9, 15		# AND input with 15, set into $t0
		add	$t9, $zero, $zero	# Reset $t9 back to 0 after we're done parsing input
		slti	$t1, $t5, 14		# If input is less than 14, set $t1 to true
		beq	$t1, 0,	st2_clrEq	# If input >= 14, go to st1_clrEq (pressed clear or equals)
		slti	$t1, $t5, 10		# If input code is < 10, set $t1 to true
		beq	$t1, 0, st2_op		# If input code >= 10, go to st2_op (pressed an operator)
st2_number:	sll	$t2, $s1, 3		# ($s1 << 3), store in $t2
		sll	$t3, $s1, 1		# ($s1 << 1), store in $t3
		add	$s1, $t2, $t3		# Add ($s1 << 3) + ($s1 << 1)
		add	$s1, $s1, $t5		# Add input to $s1
		add	$t8, $zero, $s1		# Display operand 2
		j	state3			# Jump to state3
st2_op:		add	$s2, $zero, $t5		# Copy operator code to $s2
		add	$t8, $zero, $s0		# Display operand 1
		j	state2			# Jump back to state2
st2_clrEq:	beq	$t5, 14, st2_equals	# If input is 15, go to clear
		j	state0			# Jump to state0
st2_equals:	add	$s3, $zero, $s0		# Set result as operand1
		add	$t8, $zero, $s3		# Display result
		j	state4			# Jump to state4

#### State 3 ####
state3:		beq	$t9, $zero, state3	# Keep waiting for input
		andi	$t5, $t9, 15		# AND input with 15, set into $t5
		add	$t9, $zero, $zero	# Reset $t9 back to 0 after we're done parsing input
		slti	$t1, $t5, 14		# If input is less than 14, set $t1 to true
		beq	$t1, 0,	st3_clrEq	# If input >= 14, go to st1_clrEq (pressed clear or equals)
		slti	$t1, $t5, 10		# If input code is < 10, set $t1 to true
		beq	$t1, 0, st3_op		# If input code >= 10, go to st2_op (pressed an operator)
st3_number:	sll	$t2, $s1, 3		# ($s1 << 3), store in $t2
		sll	$t3, $s1, 1		# ($s1 << 1), store in $t3
		add	$s1, $t2, $t3		# Add ($s1 << 3) + ($s1 << 1)
		add	$s1, $s1, $t5		# Add input to $s1
		add	$t8, $zero, $s1		# Display operand 2
		j	state3			# Jump to state3	
st3_op:		beq	$s2, 10, st3_op_add	# Jump to add logic if operator == 11
		beq	$s2, 11, st3_op_sub	# Jump to subtract logic if operator == 12
		beq	$s2, 12, st3_op_mult	# Jump to multiply logic if operator == 13
		beq	$s2, 13, st3_op_div	# Jump to divide logic if operator == 14
st3_op_add:	add	$s3, $s0, $s1		# Add numbers
		j	st3_disp
st3_op_sub:	sub	$s3, $s0, $s1		# Subtract numbers
		j	st3_disp
st3_op_mult:	add	$t0, $zero, $s0		# Multiply numbers - Copy operand1 for calculation
		add	$t1, $zero, $s1		# Copy operand2 for calculation
		add	$t4, $zero, $zero	# Reset $t4 to zero
		add	$t2, $zero, $zero	# Reset temp $t2 ?????
		add	$t3, $zero, $zero	# $t3 = temp shift counter
 st3_m_lp:	beq	$t1, 0, st3_disp	# While opeand2 != 0
		andi	$t2, $t1, 1		# Checking first bit of operand2, $t2 is temp
		beq	$t2, 0, shiftOp2	# If first bit of operand2 == 0 skip ahead
		sllv	$t4, $t0, $t3		# Shift operand1 left $t3 times
		add	$s3, $s3, $t4		# Add $t4 to result
 shiftOp2:	add	$t3, $t3, 1		# Add 1 to shift counter
		srl	$t1, $t1, 1		# Right shift operand2 by 1
		j	st3_m_lp		# Jump back up to top of loop
st3_op_div:	add	$t0, $zero, $s0		# Divide numbers - Copy operand1 for calculation
		add	$t1, $zero, $s1		# Copy operand2 for calculation
		add	$t2, $zero, $s0		# Let $t2 = temporary operand1
		add	$t3, $zero, $zero	# Let $t3 = counter. Will be our result
		add	$t4, $zero, $zero	# temp for comparison
st3_d_lp:	sub	$t0, $t0, $t1		# Subtract operand2 from operand1
		slti	$t4, $t0, 0		# Is operand1 < 0? Set t/f to temp $t4
		beq	$t4, 1, st3_d_done	# If so, we are done
		addi	$t3, $t3, 1		# Add 1 to the counter
		j	st3_d_lp		# Continue division loop
st3_d_done:	add	$s3, $zero, $t3		# Add result of division to result
st3_disp:	add	$t8, $zero, $s3		# Display result
		add	$s0, $zero, $s3		# Set operand1 to result
		add	$s1, $zero, $zero	# Reset operand2
		add	$s2, $zero, $t5		# Set operator to input
		j	state2			# Jump to state2 
st3_clrEq:    	beq	$t5, 14, st3_equals	# Jump to st3_equals if $t9 == 14
		j	state0			# Jump to state0, assume $t9 is 15
st3_equals:	beq	$s2, 10, st3_eq_add	# Jump to add logic if operator == 11
		beq	$s2, 11, st3_eq_sub	# Jump to subtract logic if operator == 12
		beq	$s2, 12, st3_eq_mult	# Jump to multiply logic if operator == 13
		beq	$s2, 13, st3_eq_div	# Jump to divide logic if operator == 14
st3_eq_add:	add	$s3, $s0, $s1		# Add numbers
		j	st3_disp_eq
st3_eq_sub:	sub	$s3, $s0, $s1		# Subtract numbers
		j	st3_disp_eq
st3_eq_mult:	add	$t0, $zero, $s0		# Copy operand1 for calculation
		add	$t1, $zero, $s1		# Copy operand2 for calculation
		add	$t3, $zero, $zero	# $t3 = temp shift counter
		add	$t4, $zero, $zero	# Reset $t4 to zero
		add	$s3, $zero, $zero	# Reset result to zero
 st3_eq_m_lp:	beq	$t1, 0, st3_disp_eq	# While operand2 != 0
		andi	$t2, $t1, 1		# Checking first bit of operand2, $t2 is temp
		beq	$t2, 0, eq_shiftOp2	# If first bit of operand2 == 0 skip ahead
		sllv	$t4, $t0, $t3		# Shift operand1 left $t3 times
		add	$s3, $s3, $t4		# Add $t4 to result
 eq_shiftOp2:   add	$t3, $t3, 1		# Add 1 to shift counter
		srl	$t1, $t1, 1		# Right shift operand2 by 1
		j	st3_eq_m_lp		# Jump back up to top of loop		
st3_eq_div:	add	$t0, $zero, $s0		# Copy operand1 for calculation
		add	$t1, $zero, $s1		# Copy operand2 for calculation
		add	$t2, $zero, $s0		# Let $t2 = temporary operand1
		add	$t3, $zero, $zero	# Let $t3 = counter. Will be our result
		add	$t4, $zero, $zero	# temp for comparison
 st3_eq_d_lp:	sub	$t0, $t0, $t1		# Subtract operand2 from operand1
		slti	$t4, $t0, 0		# Is operand1 < 0? Set t/f to temp $t4
		beq	$t4, 1, st3_eq_ddone	# If so, we are done
		addi	$t3, $t3, 1		# Add 1 to the counter
		j	st3_eq_d_lp		# Continue division loop
 st3_eq_ddone:	add	$s3, $zero, $t3		# Add result of division to result
st3_disp_eq:	add	$t8, $zero, $s3		# Display result
		add	$s1, $zero, $zero	# Reset operand2
		j	state4			# Jump to state4 

#### State 4 ####
state4:		beq	$t9, $zero, state4	# Loop for input
		andi	$t5, $t9, 15		# AND input with 15, set into $t5
		add	$t9, $zero, $zero	# Reset $t9 back to 0
		slti	$t1, $t5, 14		# If input is less than 14, set $t1 to true
		beq	$t1, 0,	st4_clrEq	# If input >= 14, go to st4_clrEq (pressed clear or equals)
		slti	$t1, $t5, 10		# If input code is < 10, set $t1 to true
		beq	$t1, 0, st4_op		# If input code >= 10, go to st4_op
st4_number:	add	$s0, $zero, $t5		# Set operand1 to input
		add	$t8, $zero, $s0		# Display operand1
		j	state1			# Jump to state1
st4_op:		add	$s0, $zero, $s3		# Set operand1 to result
		add	$s2, $zero, $t5		# Set operator to input
		j	state2			# Jump to state2
st4_clrEq:	beq	$t5, 14, st4_equals	# If input is 14 jump to st4_equals
		j	state0			# Jump back to state0
st4_equals:	add	$t8, $zero, $s3		# Display result
		j	state4			# Jump back to state4
