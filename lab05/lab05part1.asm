.data
	types: 	.asciiz "bit", "nybble", "byte", "half", "word"
	bits: 	.asciiz "one", "four", "eight", "sixteen", "thirty-two"
	prompt:	.asciiz	"Please enter a datatype:\n"
	out:	.asciiz "\nNumber of bits: "
	notFnd:	.asciiz "\nNot found!"
	input:	.space 64

.text
		addi	$v0, $zero, 4		# Add 4 to $v0 for print string
		la	$a0, prompt		# Load prompt
		syscall				# Print prompt
	
		la	$a0, input		# Load addr of input buffer
		addi	$a1, $zero, 64		# Max input size
		jal	_readString		# Read in string
	
		la	$a0, input		# Load addr of input buffer
		la	$a1, types		# Load addr of "types" buffer
		add	$t0, $zero, $zero	# $t0 = offset
	
lp_checkType:	la	$a0, input		# Load addr of input buffer
		la	$a1, $t0($a1)		# Load address of "type" string to check against
		jal	_checkType		# Call _checkType
		add	$t1, $zero, $v0		# Copy return value of _checkType
		beq	$t1, 1, match		# If match, jump to match
		add	$a0, $zero, $a1		# Load "types" string into $a0 for _strLength
		jal	_strLength		# Find length of current "types" string
		add	$t0, $t0, $v0		# Add string length to offset
		j	lp_checkType		# Loop through "types" array
		
match:
	
##-----------------------------------------------##
# _checkType -  compare name with input string
#
# Args:		$a0 - address of an input buffer
#		$a1 - address of string to compare
#
# Returns:	$v0 - 1 if they match, 0 if not
#------------------------------------------------##
_checkType:	addi	$sp, $sp, -4		# Make room for $ra on stack
		sw	$ra, 0($sp)		# Store return address on stack
		
		add	$t0, $zero, $a0		# Copy $a0 to $t0
		add	$t1, $zero, $a1		# Copy $a1 to $t1
		jal	_strLength		# Call _strLength for length of input
		add	$t2, $zero, $v0		# $t2 = length of input
		add	$a0, $zero, $t1		# Load $t1 to $a0
		jal	_strLength		# Call _strLength for length of "types" string
		add	$t3, $zero, $v0		# $t3 = length of string to check against
		bne	$t2, $t3, _cT_ne	# If they don't match length, skip comparison, return 0
_cT_comp:	beq	$t0, $zero, _cT_eq	# If we reach null char, they are equal
		lb	$t4, ($t0)		# Load current byte of $t0
		lb	$t5, ($t1)		# Load current byte of $t1
		bne	$t4, $t5, _cT_ne	# If byte doesn't match, return 0
		addi	$t0, $t0, 1		# Go to next byte of input
		addi	$t1, $t1, 1		# Go to next byte of string
		j	_cT_comp		# Loop, keep comparing bytes of strings
_cT_ne:		add 	$v0, $zero, $zero	# Return 0 if they don't match
		j	_cT_ret			# Jump to return
_cT_eq:		addi	$v0, $zero, 1		# Strings are equal, return 1

_cT_ret:	lw	$ra, 0($sp)		# Load return addr from stack
		add	$sp, $sp, 4		# Adjust stack back
		jr	$ra			# Go back to caller

##-----------------------------------------------##
# _lookUp   -	Find string in array of strings
#		via index 
#
# Args:		$a0 - address of string array
#		$a1 - index to look at
#
# Returns:	$v0 - address of string at index
#------------------------------------------------##
_lookUp:
.text
		


##-----------------------------------------------##
# _readString - read null-terminated string 
#		into memory from user input
#
# Args:		$a0 - address of an input buffer
#		$a1 - max number of chars to read in
#
# Returns:	None
#------------------------------------------------##
_readString:
.text
			add	$t9, $zero, $ra		# Save the register we have to jump back to
			add	$v0, $zero, 8		# Add to $v0 to get input string
			la	$a0, input		# Load addr of buffer to $a0 to store input there
			addi	$a1, $zero, 64		# Specify max size of input, store in $a1 for syscall
			syscall				# Get input
			jal	_strLength
			add	$t0, $zero, $v0		# Copy return value from _strLength to $t0
			addi	$t0, $t0, -1		# Offset to '\n' character, store index number
			add	$s0, $zero, $t0		# Length of string -> $s0 for later
			add	$t1, $t0, $a0		# Address of the byte we want to change	in $t1	
			sb	$zero, ($t1) 		# Set the byte at ($t1) to zero (NULL character \0)
		
			add	$ra, $zero, $t9		# Set the return address back
			jr	$ra			# Return


##------------------------------------------##
# _strLength - get length of a string
#
# Args:		$a0 - address of string
# Returns:	$v0 - length of the string
#-------------------------------------------##
_strLength:
.text
			add	$t1, $zero, $zero	# $t1 = counter. Return this value later
			add	$t2, $zero, $zero	# $t2 = current byte
	lp_strLength:	add	$t0, $a0, $t1		# Add offset to current address, store in $t0 		
			lbu	$t2, ($t0)		# Load current byte into $t2.
			beq	$t2, 0, ret_strLength	# Branch if we've hit the null character
			addi	$t1, $t1, 1		# Add 1 to counter
			j	lp_strLength		# Jump back up to top of loop, keep counting
	ret_strLength:	add	$v0, $zero, $t1		# Set return value $t1 to $v0
			jr	$ra			# Return
